#ifndef NAMELISTTESTHARNESS_H
#define NAMELISTTESTHARNESS_H

#include <limits.h>
#include <gtest/gtest.h>

#include <memory>

using std::shared_ptr;

class NameList;

class NameListTestHarness : public ::testing::Test
{
protected:
    virtual void SetUp();
    virtual void TearDown();

    shared_ptr<NameList> nameList;

public:
    NameListTestHarness();
    virtual ~NameListTestHarness();
};

#endif // CUBETESTHARNESS_H
