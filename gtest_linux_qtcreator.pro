#-------------------------------------------------
#
# Project created by QtCreator 2016-11-01T19:17:05
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

# Passing this in on the commandline, this allows
# building from bitbucket
# e.g. qmake-qt4 GroupMaker.pro CONFIG+=runtime

CONFIG += test

DEPENDPATH += .
INCLUDEPATH += .

#for gtest testing
LIBS += -lgtest \
        -lgtest_main \
        -lpthread

SOURCES += NameList.cpp
HEADERS  += NameList.h

runtime{
TARGET = GroupMaker
SOURCES += main.cpp

}

test{
TARGET = gtest
SOURCES += gtestMain.cpp \
    NameListTestHarness.cpp
HEADERS += NameListTestHarness.h
}
