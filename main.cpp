#include "NameList.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::cout << "Fake application to demonstrate separate test/runtime builds" << std::endl;

    return a.exec();
}
